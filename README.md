# Animated Matrix Traversal

Based on DFS (Depth First Search) algorithm. Program uses SFML library to visually represent the algorithm impelementation on a matrix.

To compile it you will need to have SFML preinstalled.

```
$ cd bin
$ g++ -c ../src/*.cc
$ g++ *.o -o squares -lsfml-window -lsfml-graphics -lsfml-system
$ ./squares
```

Use your mouse to click on fields you want to omit and hit the space to start the traversal.

To use it without compiling just execute the precompiled binary (bin/squares).