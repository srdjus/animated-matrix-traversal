#include "map.h"

Map::Map(unsigned i, unsigned j) {
    fields.assign(i, std::vector<int>(j));

    height = i;
    width = j;
}

void Map::load(unsigned t_size) {
    s_size = t_size;
    s_vertices.setPrimitiveType(sf::Quads);
    s_vertices.resize(width * height * 4);

    update(); 
}

void Map::update() {

    for (int i = 0; i < fields.size(); i++) {

        for (int j = 0; j < fields[0].size(); j++) {
            sf::Vertex* quad = &s_vertices[(i * width + j) * 4];

            quad[0].position = sf::Vector2f(j * s_size, i * s_size);
            quad[1].position = sf::Vector2f((j + 1) * s_size, i * s_size);
            quad[2].position = sf::Vector2f((j + 1) * s_size, (i + 1) * s_size);
            quad[3].position = sf::Vector2f(j * s_size, (i + 1) * s_size);

            switch(fields[i][j]) {
                case 0:

                    for (int k = 0; k < 4; k++) {
                        quad[k].color = sf::Color::White;
                    }

                    break;
                case 1:

                    for (int k = 0; k < 4; k++) {
                        quad[k].color = sf::Color::Black;
                    }

                    break;
                case 2:

                    for (int k = 0; k < 4; k++) {
                        quad[k].color = sf::Color::Magenta;
                    }

                    break;
            }
        } 
    }
}

bool Map::setField(unsigned i, unsigned j, int value) {
    if (i >= height || j >= width) {
        return false; 
    }

    fields[i][j] = value;
    return true;
}

int Map::getField(unsigned i, unsigned j) {
    return fields[i][j]; 
}

unsigned Map::getWidth() {
    return width; 
}

unsigned Map::getHeight() {
    return height; 
}

std::tuple<int, int> Map::getNeighbour(
        std::tuple<int,int> t) {

    int i = std::get<0>(t), j = std::get<1>(t);

    if (i >= fields.size() || j >= fields[0].size()) {
        return std::make_tuple(-1, -1); 
    }  

    if (j > 0 && fields[i][j - 1] == 0) {
        j--;
    } else if (j < fields[i].size() - 1 && fields[i][j + 1] == 0) {
        j++;
    } else if (i > 0 && fields[i - 1][j] == 0) {
        i--;
    } else if (i < fields.size() - 1 && fields[i + 1][j] == 0) {
        i++;
    } else {
        return std::make_tuple(-1, -1);
    }

    return std::make_tuple(i, j);
}

void Map::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    target.draw(s_vertices, states);
}