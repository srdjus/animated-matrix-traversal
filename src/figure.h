#ifndef FIGURE_HPP
#define FIGURE_HPP

#include <iostream>
#include <SFML/Graphics.hpp>

class Figure : public sf::Drawable {
    public:
        Figure(int i, int j, int size);

        void setPosition(int i, int j, int size);
    
    private:
        virtual void draw(sf::RenderTarget& target, 
                sf::RenderStates states) const;

        sf::VertexArray points;
};

#endif