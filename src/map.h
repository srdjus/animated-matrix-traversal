#ifndef MAP_HPP
#define MAP_HPP

#include <iostream>
#include <vector>
#include <tuple>
#include <SFML/Graphics.hpp>

class Map : public sf::Drawable {
    public:
        Map(unsigned i, unsigned j);

        void load(unsigned t_size);
        void update();
        bool setField(unsigned i, unsigned j, int val);
        int getField(unsigned i, unsigned j);
        unsigned getWidth();
        unsigned getHeight();
        std::tuple<int, int> getNeighbour(std::tuple<int, int> t);

    private:
        virtual void draw(sf::RenderTarget& target, 
                sf::RenderStates states) const;

        sf::VertexArray s_vertices;
        std::vector<std::vector<int>> fields;
        unsigned width, height, s_size;
};

#endif