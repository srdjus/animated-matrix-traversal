#include <iostream>
#include <vector>
#include <tuple>
#include <SFML/Graphics.hpp>

#include "map.h"
#include "figure.h"

int main() {
    sf::RenderWindow window(sf::VideoMode(640, 480), "Squares");

    Map map(15, 20);
    map.load(32);

    Figure figure(0, 0, 32);
    
    sf::Clock clock;

    std::vector<std::tuple<int, int>> stack;
    std::tuple<int, int> next;

    bool paused = true;

    stack.push_back(std::make_tuple(0, 0));

    while (window.isOpen()) {
        map.update();
        
        sf::Event e;

        while(window.pollEvent(e)) {

            if (e.type == sf::Event::Closed) {
                window.close();
            } else if (e.type == sf::Event::KeyPressed) {

                if (e.key.code == 57) paused = !paused;

            } else if (e.type == sf::Event::MouseButtonReleased) {
                map.setField(e.mouseButton.y / 32, 
                        e.mouseButton.x / 32, 2); 
            } 
        }

        sf::Time elapsed = clock.getElapsedTime();

        if (!paused && elapsed.asMilliseconds() >= 75) {

            if (stack.size()) {
                next = map.getNeighbour(stack.back());

                map.setField(std::get<0>(stack.back()), 
                        std::get<1>(stack.back()), 1);
                
                figure.setPosition(std::get<0>(stack.back()), 
                        std::get<1>(stack.back()), 32);

                if (std::get<0>(next) == -1) {
                    stack.pop_back();
                } else {
                    stack.push_back(next);
                }

            } else paused = true;

            clock.restart();
        }
        
        window.clear(sf::Color::White);
        window.draw(map);
        window.draw(figure);
        window.display();      
    }

    return 0;
}