#include "figure.h"

Figure::Figure(int i, int j, int size) {

    points.setPrimitiveType(sf::Quads);
    points.resize(4); 

    setPosition(i, j, size);

    for (int i = 0; i < 4; i++) {
        points[i].color = sf::Color::Red;
    }
}

void Figure::setPosition(int i, int j, int size) {
    points[0].position = sf::Vector2f(j * size, i * size);
    points[1].position = sf::Vector2f((j + 1) * size, i * size);
    points[2].position = sf::Vector2f((j + 1) * size, (i + 1) * size);
    points[3].position = sf::Vector2f(j * size, (i + 1) * size);
}

void Figure::draw(sf::RenderTarget& target, 
        sf::RenderStates states) const {
    target.draw(points, states);
}